#=
** This script performs tests for the trust region nonlinear solver.
**
** Copyright © 2018, 2019 Dynare Team
**
** This file is part of Dynare.
**
** Dynare is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Dynare is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Dynare.  If not, see <http://www.gnu.org/licenses/>.
=#

rootdir = @__DIR__
origdir = pwd()

include("../checkpath.jl")

using Test
using DynareSolvers
using TestFunctions

cd("$(rootdir)")

ntries = 1
n = 10

@time @testset "test trust-region-solver" begin
    factor = 1.0
    xguess = rosenbrock()
    for i=ntries
        @test begin
            x, info  = trustregion(rosenbrock!, rosenbrock!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = powell1()
    for i=ntries
        @test begin
            x, info  = trustregion(powell1!, powell1!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = powell2()
    for i=1:ntries
        @test begin
            x, info  = trustregion(powell2!, powell2!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = wood()
    for i=1:ntries
        @test begin
            x, info  = trustregion(wood!, wood!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = helicalvalley()
    for i=1:ntries
        @test begin
            x, info  = trustregion(helicalvalley!, helicalvalley!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = watson(n)
    for i=1:ntries
        @test begin
            x, info  = trustregion(watson!, watson!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = chebyquad(n)
    for i=1:ntries
        @test begin
            x, info  = trustregion(chebyquad!, chebyquad!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = brown(n)
    for i=1:ntries
        @test begin
            x, info  = trustregion(brown!, brown!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = discreteboundaryvalue(n)
    for i=1:ntries
        @test begin
            x, info  = trustregion(discreteboundaryvalue!, discreteboundaryvalue!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = discreteintegralequation(n)
    for i=1:ntries
        @test begin
            x, info  = trustregion(discreteintegralequation!, discreteintegralequation!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = trigonometric(n)
    for i=1:ntries
        @test begin
            x, info  = trustregion(trigonometric!, trigonometric!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = variablydimensioned(n)
    for i=1:ntries
        @test begin
            x, info  = trustregion(variablydimensioned!, variablydimensioned!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = broydentridiagonal(n)
    for i=1:ntries
        @test begin
            x, info  = trustregion(broydentridiagonal!, broydentridiagonal!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
    factor = 1.0
    xguess = broydenbanded(n)
    for i=1:ntries
        @test begin
            x, info  = trustregion(broydenbanded!, broydenbanded!, factor*xguess, 1.0e-6, 1.0e-6, 50*n)
            info==1
        end
        factor *= 2.0
    end
end

cd(origdir)
