#=
** This script performs tests for the PreprocessorOptions type. 
**
** Copyright (C) 2018 Dynare Team
**
** This file is part of Dynare.
**
** Dynare is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Dynare is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Dynare.  If not, see <http://www.gnu.org/licenses/>.
=#

rootdir = @__DIR__
origdir = pwd()

include("../checkpath.jl")

using Test
using Dynare
using DynareUnitTests

@testset "test preprocessor-2" begin
    @test begin
        # Test that we can instantiate an object.
        try
            opt  = Dynare.PreprocessorOptions()
            true
        catch
            false
        end
    end
    @test begin
        # Test that we cannot access a field that does not exist.
        try
            opt  = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :noddy, "toyland")
            false
        catch
            true
        end
    end
    @test begin
        # Test that we cannot assign a non expected type to a member (field).
        try
            opt  = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :debug, "true")
            false
        catch
            true
        end
    end
    @test begin
        # Test that we can change the value of an option.
        try
            opt  = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :debug, true)
            opt.debug==true
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (default option values).
        try
            opt = Dynare.PreprocessorOptions()
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed debug option).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :debug, true)
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("nopreprocessoroutput", str) && occursin("debug", str)
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed debug and noclearall options).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :debug, true)
            Dynare.PreprocessorOptions!(opt, :noclearall, true)
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("nopreprocessoroutput", str) && occursin("debug", str) && occursin("noclearall", str)
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed option savemacro).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :savemacro, "toto.mod")
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("nopreprocessoroutput", str) && occursin("savemacro=toto.mod", str)
        catch
            false
        end
    end
    for option in [:savemacro :onlymacro :nolinemacro :noemptylinemacro :notmpterms :nolog :warn_uninit :nograph :nointeractive :parallel :parallel_slave_open_mode :parallel_test :nostrict :stochastic :fast :minimal_workspace :compute_xrefs]
        @test begin
            # Test that we can print the list of options in a string.
            try
                opt = Dynare.PreprocessorOptions()
                Dynare.PreprocessorOptions!(opt, option, true)
                str = Dynare.print(opt)
                occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("nopreprocessoroutput", str) && occursin("$(string(option))", str)
            catch
                false
            end
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed option parallel).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :parallel, "mycluster")
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("nopreprocessoroutput", str) && occursin("parallel=mycluster", str)
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed option conffile).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :conffile, "/home/toto/.dynare.m")
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("nopreprocessoroutput", str) && occursin("conffile=/home/toto/.dynare.m", str)
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed option conffile).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :conffile, "/home/toto/.dynare.m")
            str = Dynare.print(opt)
            str==" conffile=/home/toto/.dynare.m output=dynamic language=julia nopreprocessoroutput" 
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed option output).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :output, "second")
            str = Dynare.print(opt)
            occursin("output=second", str) && occursin("language=julia", str) && occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed option language).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :language, "c++")
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && occursin("language=c++", str) && occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed option language).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :language, "matlab")
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && !occursin("language", str) && occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed option param_derivs_order).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :params_derivs_order, 1)
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("params_derivs_order=1", str) && occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
    @test begin
        # Test that we can print the list of options in a string (provide wrong type to option param_derivs_order).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :params_derivs_order, "one")
            false
        catch
            true
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed option json).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :json, "transform")
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("json=transform", str) && occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
    for option in [:jsonstdout :onlyjson :jsonderivsimple :nopathchange]
        @test begin
            # Test that we can print the list of options in a string.
            try
                opt = Dynare.PreprocessorOptions()
                Dynare.PreprocessorOptions!(opt, option, true)
                str = Dynare.print(opt)
                occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("$(string(option))", str) && occursin("nopreprocessoroutput", str)
            catch
                false
            end
        end
    end
    @test begin
        # Test that we can print the list of options in a string (changed nopreprocessoroutput option).
        try
            opt = Dynare.PreprocessorOptions()
            Dynare.PreprocessorOptions!(opt, :nopreprocessoroutput, false)
            str = Dynare.print(opt)
            occursin("output=dynamic", str) && occursin("language=julia", str) && !occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
    @test begin
        # Test @PreprocessorOptions (added one option)
        try
            options = Dynare.@PreprocessorOptions :debug
            str = Dynare.print(options)
            occursin("debug", str) && occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
    @test begin
        # Test @PreprocessorOptions (added two options)
        try
            options = Dynare.@PreprocessorOptions :debug :nograph
            str = Dynare.print(options)
            occursin("debug", str) && occursin("nograph", str) && occursin("output=dynamic", str) && occursin("language=julia", str) && occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
    @test begin
        # Test @PreprocessorOptions (added two options and changed lanquage value)
        try
            options = Dynare.@PreprocessorOptions :debug :nograph :language "c++"
            str = Dynare.print(options)
            occursin("debug", str) && occursin("nograph", str) && occursin("output=dynamic", str) && occursin("language=c++", str) && occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
    @test begin
        # Test @PreprocessorOptions (added two options, changed lanquage value and removed nopreprocessoroutput)
        try
            options = Dynare.@PreprocessorOptions :debug :nograph :language "c++" :nopreprocessoroutput false
            str = Dynare.print(options)
            occursin("debug", str) && occursin("nograph", str) && occursin("output=dynamic", str) && occursin("language=c++", str) && !occursin("nopreprocessoroutput", str)
        catch
            false
        end
    end
end
