## Dynare for Julia

This package aims at bringing to Julia some of the functionality provided by
[Dynare](http://www.dynare.org), a platform for solving economic models and in
particular DSGE models.

Please note that this Julia package is very incomplete compared to the original
Dynare for MATLAB/Octave, but hopefully it will become more featureful over
time.

For the moment this package is only able to compute a model’s steady state.

The package is tested against Julia 1.0.x and 1.2.x.

## Installation

Assuming you already have [julia](https://julialang.org/) (version
1.0.x), enter the Pkg REPL by pressing ] from the Julia REPL.:
```jl
    (v1.0) pkg> add https://git.dynare.org/Dynare/Dynare.jl#with-preprocessor
```
This will also install the preprocessor, selecting the binary
corresponding to your platform (Linux/Windows/MacOS). If you do not
already have them, other required packages will be fetched during the installation.

## Update

To update the julia codes and the preprocessor, again the Pkg REPL, do:
```jl
    (v1.0) pkg> up Dynare
```

## Check

The package comes with a testsuite, to check that everything is fine with the installation, do (still in the Pkg REPL):
```jl
    (v1.0) pkg> test Dynare
```
you should then obtain something like:
```jl
Test Summary:       | Pass  Total
Dynare.jl testsuite |  188    188
   Testing Dynare tests passed 
```
at the bottom of the displayed informations.
