
mutable struct UnstableSystemException <: Exception end

mutable struct UndeterminateSystemException <: Exception end
